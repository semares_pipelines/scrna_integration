// general input and params
if (params.input_files){
    input_files = params.input_files.tokenize(",").collect{file(it, checkIfExists: true)}
}

// ch_metadata_file = file(params.metadata_file, checkIfExists: true)
ch_output_folder = file(params.output_folder)

workflow INTEGRATION {
    INTEGRATION_PROCESS (
         input_files
    )
}

process INTEGRATION_PROCESS {

    container "${(params.containers_local) ? 'semares_scrna_integration:1.1.0' : 
                      'registry.gitlab.com/semares_pipelines/scrna_integration/semares_scrna_integration:1.1.0' }"

    input:
        val input_files

    output:
        path("*")
    
    script:
        """
        integration.R \\
           --input_files ${input_files.join(",")} \\
           --output_folder "./" \\
           --references ${params.references} \\
           --individual_id ${params.individual_id} \\
           --umap_resolution_value ${params.umap_resolution_value} \\
           --var_features_method ${params.var_features_method} \\
           --var_features_num ${params.var_features_num} \\
           --normalization_method ${params.normalization_method} \\
           --normalization_scale ${params.normalization_scale} \\
           --k_weight ${params.k_weight}

        """  
}

