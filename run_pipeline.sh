#docker run -v $PWD/bin:/scripts -v $PWD/test_input:/data registry.gitlab.com/semares_pipelines/scrna_demultiplexing/semares_demultiplex:1.0.0 /bin/bash -c "Rscript /scripts/demultiplex.R --input_files /data/sample-P126BK_ADT_HTO_P126BK_GEX_filtered_feature_bc_matrix.h5 --output_folder /data/test_output --hashtags_file /data/Hashtags_used.csv --demult_quantile 0.99 --cells_heatmap 5000"
#nextflow main.nf --input_files ./test_input/SeuObj_P132K.rds,./test_input/SeuObj_P133K.rds,./test_input/SeuObj_P134K.rds --output_folder ./test_output --metadata_file ./test_input/metadata.json -profile docker
./nextflow main.nf --input_files ./test_input/SeuObj_Analysed_P126K.rds,./test_input/SeuObj_Analysed_P112BK.rds,./test_input/SeuObj_Analysed_P114BK.rds \
                   --output_folder ./test_output_2 \
                   --references P126BK,P112BK \
                   --individual_id pid \
                   --umap_resolution_value 0.5 \
                   --var_features_method vst\
                   --var_features_num 2000 \
                   --normalization_method LogNormalize \
                   --normalization_scale 10000 \
                   --k_weight 100 \
                   -profile docker                 